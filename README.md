Notas extras para la instalación de un conversor (Opcional) 

- Instalar imagemagick

sudo apt-get install imagemagick

 - En caso de ser necesario y no poder convertir las imagenes a pdf editaremos la politica de privacidad:

sudo gedit /etc/ImageMagick-6/policy.xml

- Crear capeta para las nuevas imagenes (opcional si el escaneo o las fotos son en alta resolucion, la memoria caché soporta solo uos 250mb)
 
mkdir notas


ls *jpg|awk '{
split($1,filename,"."); 
name=filename[1]; 
command="convert "$1" -resize 1200x800 ./notas\/"name"_min.jpg"; 
system(command)}'

 - Dentro del archivo editar la siguente linea:

policy domain="coder" rights="none" pattern="PDF"

 - A la siguiente:

policy domain="coder" rights="read|write" pattern="PDF"

 - Guardar... Y con esto ya podemos guardar las imagenes.jpg a PDF


 - Convertir las imagenes finalmente a pdf

convert -compress jpeg *.jpg myjpegs.pdf